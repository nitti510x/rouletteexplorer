﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RouletteExplorer.Infrastructure.Extensions
{
    public static class Core
    {
        public static double PercentageOf(this int count, int total)
        {
            return Math.Round(count * 100.0 / total, 1);
        }

        public static T Prominence<T>(this IEnumerable<T> list)
        {
            return list.GroupBy(x => x)
                .OrderByDescending(x => x.Count())
                .Select(x => x.Key).First();
        }

        public static IEnumerable<T> ListProminence<T>(this IEnumerable<T> list, int count)
        {
            return list.GroupBy(x => x)
                .OrderByDescending(x => x.Count())
                .Select(x => x.Key)
                .Take(count);
        }
    }
}
