﻿using System;
using RouletteExplorer.Models.Reports;
using System.Collections.Generic;

namespace RouletteExplorer.Interfaces.Managers.Report
{
    public interface IReportManager
    {
        IEnumerable<OutsideBetReport> GetOutsideBetReport(Guid? wheelId);
        IEnumerable<InsideBetReport> GetInsideBetReport(Guid? wheelId);
        IEnumerable<StreakReport> GetStreakReport(Guid? wheelId);
    }
}
