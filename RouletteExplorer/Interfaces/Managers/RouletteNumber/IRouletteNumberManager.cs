﻿using RouletteExplorer.Models.Numbers;

namespace RouletteExplorer.Interfaces.Managers.RouletteNumber
{
    public interface IRouletteNumberManager
    {
        Number GetNumberDefinition(int number);
    }
}
