﻿using RouletteExplorer.Models.Reports;
using System.Collections.Generic;

namespace RouletteExplorer.ViewModels
{
    public class StreakReportViewModel
    {
        public string StreakReportName { get; set; }
        public IEnumerable<string> StreakReportAnchors { get; set; }
        public IEnumerable<StreakReport> StreakReport { get; set; }
    }
}
