﻿using System;
using System.Collections.Generic;
using RouletteExplorer.Models.Reports;

namespace RouletteExplorer.ViewModels
{
    public class OutsideBetReportViewModel
    {
        public string OutsideBetReportName { get; set; }
        public IEnumerable<string> OutsideBetsReportAnchors { get; set; }
        public IEnumerable<OutsideBetReport> OutsideBetReport { get; set; }
    }
}
