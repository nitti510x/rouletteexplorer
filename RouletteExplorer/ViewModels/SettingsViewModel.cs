﻿using System;
using System.Collections.Generic;

namespace RouletteExplorer.ViewModels
{
    public class SettingsViewModel
    {
        public string Title { get; set; }
        public IEnumerable<Wheel> Wheels { get; set; }
    }
}
