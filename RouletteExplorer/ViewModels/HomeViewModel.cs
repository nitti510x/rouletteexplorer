﻿using System;
using System.Collections.Generic;
using RouletteExplorer.Models.Numbers;

namespace RouletteExplorer.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Number> RecentSpins { get; set; }
    }
}
