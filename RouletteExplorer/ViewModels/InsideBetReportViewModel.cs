﻿using System;
using System.Collections.Generic;
using RouletteExplorer.Models.Reports;

namespace RouletteExplorer.ViewModels
{
    public class InsideBetReportViewModel
    {
        public string InsideBetReportName { get; set; }
        public IEnumerable<string> InsideBetsReportAnchors { get; set; }
        public IEnumerable<InsideBetReport> InsideBetReport { get; set; }
    }
}
