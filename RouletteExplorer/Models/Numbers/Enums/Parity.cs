﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteExplorer.Models.Numbers.Enums
{
    public enum Parity
    {
        Odd,
        Even,
        NotApplicable
    }
}