﻿using RouletteExplorer.Models.Numbers.Enums;

namespace RouletteExplorer.Models.Numbers
{
    public class Number
    {

        public Number(int value, Color color, Parity parity, Sector sector, Range range, Dozen dozen, Column column)
        {
            NumberValue = value;
            Color = color;
            Parity = parity;
            Range = range;
            Sector = sector;
            Dozen = dozen;
            Column = column;
        }

        public int NumberValue { get; }
        public Color Color { get; set; }
        public Parity Parity { get; }
        public Dozen Dozen { get; }
        public Range Range { get; }
        public Column Column { get; }
        public Sector Sector { get; }

    }
}
