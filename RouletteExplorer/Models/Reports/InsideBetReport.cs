﻿namespace RouletteExplorer.Models.Reports
{
    public class InsideBetReport
    {
        public int Seed { get; set; }
        public int Number { get; set; }
        public int HotNumber { get; set; }
        public double HotNumberBias { get; set; }
        public int SecondNumber { get; set; }
        public double SecondNumberBias { get; set; }
        public int ThirdNumber { get; set; }
        public double ThirdNumberBias { get; set; }
        public int FourthNumber { get; set; }
        public double FourthNumberBias { get; set; }
        public int FifthNumber { get; set; }
        public double FifthNumberBias { get; set; }
        public int SixthNumber { get; set; }
        public double SixthNumberBias { get; set; }
        public int SeventhNumber { get; set; }
        public double SeventhNumberBias { get; set; }
        public int EighthNumber { get; set; }
        public double EighthNumberBias { get; set; }
        public int NinthNumber { get; set; }
        public double NinthNumberBias { get; set; }
        public string Sector { get; set; }
        public double SectorBias { get; set; }
    }
}
