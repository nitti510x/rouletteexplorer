﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteExplorer.Models.Reports.Enums
{
    public enum ZeroBaisLevel
    {
        Low,
        Exact,
        High
    }
}
