﻿namespace RouletteExplorer.Models.Reports
{
    public class StreakReport
    {
        public int Seed { get; set; }
        public int Number { get; set; }
        public int OneToEighteenHighStreak { get; set; }
        public double OneToEighteenStreakAverage { get; set; }
        public int NineteenToThirtySixHighStreak { get; set; }
        public double NineteenToThirtySixAverageStreak { get; set; }
        public int OddHighStreak { get; set; }
        public double OddAverageStreak { get; set; }
        public int EvenHighStreak { get; set; }
        public double EvenAverageStreak { get; set; }
        public int RedHighStreak { get; set; }
        public double RedAverageStreak { get; set; }
        public int BlackHighStreak { get; set; }
        public double BlackAverageStreak { get; set; }
    }
}
