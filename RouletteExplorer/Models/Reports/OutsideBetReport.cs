﻿namespace RouletteExplorer.Models.Reports
{
    public class OutsideBetReport
    {
        public int Seed { get; set; }
        public int Number { get; set; }
        public string ZeroBiasLevel { get; set; }
        public double ZeroBias { get; set; }
        public string Color { get; set; }
        public double ColorBias { get; set; }
        public string Parity { get; set; }
        public double ParityBias { get; set; }
        public string Range { get; set; }
        public double RangeBias { get; set; }
        public string Dozen { get; set; }
        public double DozenBias { get; set; }
        public string Column { get; set; }
        public double ColumnBias { get; set; }
    }
}
