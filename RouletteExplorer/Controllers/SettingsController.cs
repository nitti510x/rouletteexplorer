﻿using Microsoft.AspNetCore.Mvc;
using RouletteExplorer.ViewModels;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteExplorer.Controllers
{
    public class SettingsController : Controller
    {
        private readonly RouletteExplorerContext _context;
        private const string WheelId = "A7CCABF3-441B-4637-8180-BB5D7818EC82";

        public SettingsController(RouletteExplorerContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {

            var viewModel = new SettingsViewModel
            {
                Title = "Tracked Wheels",
                Wheels = _context.Wheels
                    .Where(x => !x.IsDeleted)
                    .Select(x => new Wheel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        IsActive = x.IsActive,
                        Created = x.Created,
                        Spins = _context.Spin.Where(y => y.WheelId.Equals(x.Id)).ToList()
                    })
                    .OrderByDescending(w => w.IsActive).ThenByDescending(z => z.Created).ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name")] Wheel wheel)
        {
            if (ModelState.IsValid)
            {
                var newWheel = new Wheel()
                {
                    Id = Guid.NewGuid(),
                    Name = wheel.Name,
                    Created = DateTime.Now
                };
                _context.Add(newWheel);

                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View("Index");
        }


        public async Task<IActionResult> Delete([Bind("Id")] Guid id)
        {
            try
            {
                var wheel = _context.Wheels.SingleOrDefault(x => x.Id.Equals(id));

                if (wheel != null)
                {
                    wheel.IsActive = false;
                    wheel.IsDeleted = true;

                    _context.Update(wheel);

                    await _context.SaveChangesAsync();
                }
            }
            catch 
            {
                // suppress
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Activate([Bind("Id")] Guid id)
        {
            try
            {
                await using RouletteExplorerContext db = _context;

                List<Wheel> wheels = db.Wheels.ToList();

                wheels.ForEach(a => a.IsActive = false);

                Wheel wheel = _context.Wheels.SingleOrDefault(x => x.Id.Equals(id));

                if (wheel != null)
                {
                    wheel.IsActive = true;
                    db.Update(wheel);
                }

                await db.SaveChangesAsync();
            }
            catch
            {
                // suppress
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult AddWheel()
        {

            return View("Index");
        }

    }
}
