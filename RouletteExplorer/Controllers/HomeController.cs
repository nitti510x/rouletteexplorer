﻿using Microsoft.AspNetCore.Mvc;
using RouletteExplorer.Interfaces.Managers.RouletteNumber;
using RouletteExplorer.Models.Numbers;
using RouletteExplorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RouletteExplorer.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRouletteNumberManager _rouletteNumberManager;
        private readonly RouletteExplorerContext _context;
        private readonly Guid? _wheelId;

        public HomeController(IRouletteNumberManager rouletteNumberManager, RouletteExplorerContext context)
        {
            _rouletteNumberManager = rouletteNumberManager;
            _context = context;
            _wheelId = _context.Wheels.SingleOrDefault(x => x.IsActive)?.Id;
        }

        public IActionResult Index()
        {
            var viewModel = new HomeViewModel
            {
                RecentSpins = this.GetRecentSpins()
            };

            return View(viewModel);
        }

        [HttpGet]
        public IActionResult Record(string spin)
        {

            ICollection<int> records = spin
                .Replace(" ", "")
                .Split(',')
                .Select(int.Parse)
                .Where(s => s <= 37)
                .ToList();

            if (records.Any() && _wheelId != null)
            {
                foreach(var number in records)
                {
                    Number current = _rouletteNumberManager.GetNumberDefinition(number);

                    var entry = new Spin
                    {
                        WheelId = (Guid)_wheelId,
                        Number = number,
                        Color = current.Color.ToString(),
                        Parity = current.Parity.ToString(),
                        Range = current.Range.ToString(),
                        Dozen = current.Dozen.ToString(),
                        Column = current.Column.ToString(),
                        Sector = current.Sector.ToString(),
                        Created = DateTime.Now
                    };

                    _context.Add(entry);

                    _context.SaveChanges();

                }

            }


            var viewModel = new HomeViewModel
            {
                RecentSpins = this.GetRecentSpins()
            };

            return View("Index", viewModel);

        }

        [HttpGet]
        public IActionResult Undo()
        {
            Spin last = _context.Spin.ToList()
                .LastOrDefault(s => s.WheelId.Equals(_wheelId));

            if (last != null)
            {
                _context.Spin.Remove(last);
                _context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        private IEnumerable<Number> GetRecentSpins(int count = 0)
        {
            var spins = _context.Spin;

            return spins.Where(s => s.WheelId.Equals(_wheelId))
                .OrderByDescending(x => x.Seed)
                .Select(s => _rouletteNumberManager.GetNumberDefinition(s.Number))
                .Take(count ==0 ? spins.Count() : count);
        }
    }
}
