﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RouletteExplorer.Interfaces.Managers.Report;
using RouletteExplorer.ViewModels;

namespace RouletteExplorer.Controllers
{
    public class ReportsController : Controller
    {
        private readonly IReportManager _reportManager;
        private readonly Guid? _wheelId;


        public ReportsController(IReportManager reportManager, RouletteExplorerContext context)
        {
            _reportManager = reportManager;
            _wheelId = context.Wheels.SingleOrDefault(x => x.IsActive)?.Id;
        }

        public IActionResult InsideBetReport()
        {

            var viewModel = new InsideBetReportViewModel
            {
                InsideBetReportName = "Inside Bet Report",
                InsideBetReport = _reportManager.GetInsideBetReport(_wheelId)
            };

            return View(viewModel);
        }

        public IActionResult OutsideBetReport()
        {

            var viewModel = new OutsideBetReportViewModel
            {
                OutsideBetReportName = "Outside Bet Report",
                OutsideBetReport = _reportManager.GetOutsideBetReport(_wheelId),
            };

            return View(viewModel);
        }

        public IActionResult StreakReport()
        {

            var viewModel = new StreakReportViewModel
            {
                StreakReportName = "Streak Report",
                StreakReport = _reportManager.GetStreakReport(_wheelId)
            };

            return View(viewModel);
        }

    }
}
