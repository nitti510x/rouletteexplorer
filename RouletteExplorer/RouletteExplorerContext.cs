﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace RouletteExplorer
{
    public class RouletteExplorerContext : DbContext
    {

        public DbSet<Wheel> Wheels { get; set; }
        public DbSet<Spin> Spin { get; set; }

        public RouletteExplorerContext(DbContextOptions<RouletteExplorerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Wheel>().ToTable("Wheel");
            modelBuilder.Entity<Spin>().ToTable("Spin");
            modelBuilder.Entity<Spin>()
                .HasKey(s => new { s.Seed });
        }
    }

    public class Wheel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public List<Spin> Spins { get; set; } = new List<Spin>();
    }

    public class Spin
    {
        public int Seed { get; set;  }
        public Guid WheelId { get; set; }
        public int Number { get; set; }
        public string Color { get; set; }
        public string Parity { get; set; }
        public string Range { get; set; }
        public string Dozen { get; set; }
        public string Column { get; set; }
        public string Sector { get; set; }
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }
    }

}
