﻿using RouletteExplorer.Infrastructure.Extensions;
using RouletteExplorer.Interfaces.Managers.Report;
using RouletteExplorer.Models.Reports;
using RouletteExplorer.Models.Reports.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RouletteExplorer.Managers.Report
{
    public class ReportManager : IReportManager
    {
        private readonly RouletteExplorerContext _context;

        public ReportManager(RouletteExplorerContext context)
        {
            _context = context;
        }

        public IEnumerable<InsideBetReport> GetInsideBetReport(Guid? wheelId)
        {
            if(!wheelId.HasValue)
                return new InsideBetReport[0];

            IList<Spin> spins = _context.Spin
                .Where(s => s.WheelId.Equals((Guid)wheelId))
                .OrderByDescending(s => s.Seed)
                .ToList();

            if (!spins.Any())
                return new List<InsideBetReport>();

            IList<InsideBetReport> insideBetReports = new List<InsideBetReport>();

            int i = spins.Count - 1;
            while (spins.Any())
            {
                var report = new InsideBetReport
                {

                    Seed = i + 1,
                    Number = spins[i].Number
                };

                this.GetNumberProminence(ref report, spins);
                this.GetSectorProminence(ref report, spins);


                insideBetReports.Add(report);

                spins.RemoveAt(i);
                i--;
            }


            return insideBetReports;
        }

        public IEnumerable<OutsideBetReport> GetOutsideBetReport(Guid? wheelId)
        {
            if (!wheelId.HasValue)
                return new OutsideBetReport[0];

            IList<Spin> spins = _context.Spin
                .Where(s => s.WheelId.Equals((Guid)wheelId))
                .OrderByDescending(s => s.Seed)
                .ToList();

            if(!spins.Any())
                return new List<OutsideBetReport>();


            IList<OutsideBetReport> outsideBetReports = new List<OutsideBetReport>();

            int i = spins.Count - 1;
            while(spins.Any())
            {
                var report = new OutsideBetReport
                {
                    
                    Seed = i+1,
                    Number = spins[i].Number
                };

                this.GetZeroBias(ref report, spins);
                this.GetColorProminence(ref report, spins);
                this.GetParityProminence(ref report, spins);
                this.GetRangeProminence(ref report, spins);
                this.GetDozenProminence(ref report, spins);
                this.GetColumnProminence(ref report, spins);


                outsideBetReports.Add(report);

                spins.RemoveAt(i);
                i--;
            }

            return outsideBetReports;
        }

        public IEnumerable<StreakReport> GetStreakReport(Guid? wheelId)
        {

            IList<Spin> spins = _context.Spin
                .Where(s => s.WheelId.Equals((Guid)wheelId))
                .OrderByDescending(s => s.Seed)
                .ToList();

            if (!spins.Any())
                return new List<StreakReport>();


            IList<StreakReport> streakReports = new List<StreakReport>();

            int i = spins.Count - 1;
            while (spins.Any())
            {
                var report = new StreakReport
                {

                    Seed = i + 1,
                    Number = spins[i].Number
                };

                //this.GetRangeStreaks(ref report, spins);
                //this.GetParityStreaks(ref report, spins);
                //this.GetColorStreaks(ref report, spins);

                streakReports.Add(report);

                spins.RemoveAt(i);
                i--;
            }

            return streakReports.Reverse(); //reverse
        }


        #region Private methods
        private void GetNumberProminence(ref InsideBetReport insideBetReport, ICollection<Spin> spins)
        {
            IList<int> hotNumbers = spins.Select(x => x.Number).ListProminence(9).ToList();

            int totalSpins = spins.Count;

            if (hotNumbers.Any())
            {
                // 1st
                insideBetReport.HotNumber = hotNumbers[0];
                insideBetReport.HotNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[0])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 1)
            {
                // 2nd
                insideBetReport.SecondNumber = hotNumbers[1];
                insideBetReport.SecondNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[1])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 2)
            {
                // 3nd
                insideBetReport.ThirdNumber = hotNumbers[2];
                insideBetReport.ThirdNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[2])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 3)
            {
                // 4th
                insideBetReport.FourthNumber = hotNumbers[3];
                insideBetReport.FourthNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[3])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 4)
            {
                // 5th
                insideBetReport.FifthNumber = hotNumbers[4];
                insideBetReport.FifthNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[4])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 5)
            {
                // 6th
                insideBetReport.SixthNumber = hotNumbers[5];
                insideBetReport.SixthNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[5])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 6)
            {
                // 7th
                insideBetReport.SeventhNumber = hotNumbers[6];
                insideBetReport.SeventhNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[6])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 7)
            {
                // 8th
                insideBetReport.EighthNumber = hotNumbers[7];
                insideBetReport.EighthNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[7])).PercentageOf(totalSpins);
            }

            if (hotNumbers.Count > 8)
            {
                // 9th
                insideBetReport.NinthNumber = hotNumbers[8];
                insideBetReport.NinthNumberBias = spins.Count(x => x.Number.Equals(hotNumbers[8])).PercentageOf(totalSpins);
            }


        }

        private void GetSectorProminence(ref InsideBetReport insideBetReport, ICollection<Spin> spins)
        {
            string sector = spins.Select(x => x.Sector).Prominence();

            insideBetReport.Sector = sector;
            insideBetReport.SectorBias = spins.Count(x => x.Sector.Equals(sector)).PercentageOf(spins.Count);
        }

        private void GetZeroBias(ref OutsideBetReport outsideBetReport, ICollection<Spin> spins)
        {

            int zeroCount = spins.Count(z => z.Number.Equals(0));
            double zeroPercentage = zeroCount.PercentageOf(spins.Count);

            outsideBetReport.ZeroBias = zeroPercentage;


            double houseEdge = 2.7;
            if (zeroPercentage.Equals(houseEdge))
            {
                outsideBetReport.ZeroBiasLevel = ZeroBaisLevel.Exact.ToString();
            }
            else if (zeroPercentage < houseEdge)
            {
                outsideBetReport.ZeroBiasLevel = ZeroBaisLevel.Low.ToString();
            }
            else
            {
                outsideBetReport.ZeroBiasLevel = ZeroBaisLevel.High.ToString();
            }
        }

        private void GetColorProminence(ref OutsideBetReport outsideBetReport, ICollection<Spin> spins)
        {
            string color = spins.Select(x => x.Color).Prominence();

            outsideBetReport.Color = color;
            outsideBetReport.ColorBias = spins.Count(x => x.Color.Equals(color)).PercentageOf(spins.Count);
        }

        private void GetParityProminence(ref OutsideBetReport outsideBetReport, ICollection<Spin> spins)
        {
            string parity = spins.Select(x => x.Parity).Prominence();

            outsideBetReport.Parity = parity;
            outsideBetReport.ParityBias = spins.Count(x => x.Parity.Equals(parity)).PercentageOf(spins.Count);
        }

        private void GetRangeProminence(ref OutsideBetReport outsideBetReport, ICollection<Spin> spins)
        {
            string range = spins.Select(x => x.Range).Prominence();

            outsideBetReport.Range = range;
            outsideBetReport.RangeBias = spins.Count(x => x.Range.Equals(range)).PercentageOf(spins.Count);
        }

        private void GetDozenProminence(ref OutsideBetReport outsideBetReport, ICollection<Spin> spins)
        {
            string dozen = spins.Select(x => x.Dozen).Prominence();

            outsideBetReport.Dozen = dozen;
            outsideBetReport.DozenBias = spins.Count(x => x.Dozen.Equals(dozen)).PercentageOf(spins.Count);
        }

        private void GetColumnProminence(ref OutsideBetReport outsideBetReport, ICollection<Spin> spins)
        {
            string column = spins.Select(x => x.Column).Prominence();

            outsideBetReport.Column = column;
            outsideBetReport.ColumnBias = spins.Count(x => x.Column.Equals(column)).PercentageOf(spins.Count);
        } 
        #endregion
    }
}
