﻿using RouletteExplorer.Interfaces.Managers.RouletteNumber;
using RouletteExplorer.Models.Numbers;
using RouletteExplorer.Models.Numbers.Enums;
using System.Collections.Generic;
using System.Linq;

namespace RouletteExplorer.Managers.RouletteNumber
{
    public class RouletteNumberManager : IRouletteNumberManager
    {
        private readonly IList<Number> _numbers;

        public RouletteNumberManager()
        {
            _numbers = this.GetNumbersList();
        }

        public Number GetNumberDefinition(int number)
        {
            return _numbers.First(n => n.NumberValue.Equals(number));
        }

        private IList<Number> GetNumbersList()
        {
            IList<Number> numbers = new List<Number>
            {
                new Number(0, Color.NotApplicable, Parity.NotApplicable, Sector.ZeroGame, Range.NotApplicable, Dozen.NotApplicable, Column.NotApplicable),
                new Number(32, Color.Red, Parity.Even, Sector.ZeroGame, Range.High, Dozen.Third, Column.Second),
                new Number(15, Color.Black, Parity.Odd, Sector.ZeroGame, Range.Low, Dozen.Second, Column.First),
                new Number(19, Color.Red, Parity.Odd, Sector.NeighborsOfZero, Range.High, Dozen.Second, Column.First),
                new Number(4, Color.Black, Parity.Even, Sector.NeighborsOfZero, Range.Low, Dozen.First, Column.First),
                new Number(21, Color.Red, Parity.Odd, Sector.NeighborsOfZero, Range.High, Dozen.Second, Column.Third),
                new Number(2, Color.Black, Parity.Even, Sector.NeighborsOfZero, Range.Low, Dozen.First, Column.Second),
                new Number(25, Color.Red, Parity.Odd, Sector.NeighborsOfZero, Range.High, Dozen.Third, Column.First),
                new Number(17, Color.Black, Parity.Odd, Sector.Orphan, Range.Low, Dozen.Second, Column.Second),
                new Number(34, Color.Red, Parity.Even, Sector.Orphan, Range.High, Dozen.Third, Column.First),
                new Number(6, Color.Black, Parity.Even, Sector.Orphan, Range.Low, Dozen.First, Column.Third),
                new Number(27, Color.Red, Parity.Odd, Sector.ThirdOfTheWheel, Range.High, Dozen.Third, Column.Third),
                new Number(13, Color.Black, Parity.Odd, Sector.ThirdOfTheWheel, Range.Low, Dozen.Second, Column.First),
                new Number(36, Color.Red, Parity.Even, Sector.ThirdOfTheWheel, Range.High, Dozen.Third, Column.Third),
                new Number(11, Color.Black, Parity.Odd, Sector.ThirdOfTheWheel, Range.Low, Dozen.First, Column.Second),
                new Number(30, Color.Red, Parity.Even, Sector.ThirdOfTheWheel, Range.High, Dozen.Third, Column.Third),
                new Number(8, Color.Black, Parity.Even, Sector.ThirdOfTheWheel, Range.Low, Dozen.First, Column.Second),
                new Number(23, Color.Red, Parity.Odd, Sector.ThirdOfTheWheel, Range.High, Dozen.Second, Column.Second),
                new Number(10, Color.Black, Parity.Even, Sector.ThirdOfTheWheel, Range.Low, Dozen.First, Column.First),
                new Number(5, Color.Red, Parity.Odd, Sector.ThirdOfTheWheel, Range.Low, Dozen.First, Column.Second),
                new Number(24, Color.Black, Parity.Even, Sector.ThirdOfTheWheel, Range.High, Dozen.Second, Column.Third),
                new Number(16, Color.Red, Parity.Even, Sector.ThirdOfTheWheel, Range.Low, Dozen.Second, Column.First),
                new Number(33, Color.Black, Parity.Odd, Sector.ThirdOfTheWheel, Range.High, Dozen.Third, Column.Third),
                new Number(1, Color.Red, Parity.Odd, Sector.Orphan, Range.Low, Dozen.First, Column.First),
                new Number(20, Color.Black, Parity.Even, Sector.Orphan, Range.High, Dozen.Second, Column.Second),
                new Number(14, Color.Red, Parity.Even, Sector.Orphan, Range.Low, Dozen.Second, Column.Second),
                new Number(31, Color.Black, Parity.Odd, Sector.Orphan, Range.High, Dozen.Third, Column.First),
                new Number(9, Color.Red, Parity.Odd, Sector.Orphan, Range.Low, Dozen.First, Column.Third),
                new Number(22, Color.Black, Parity.Even, Sector.NeighborsOfZero, Range.High, Dozen.Second, Column.First),
                new Number(18, Color.Red, Parity.Even, Sector.NeighborsOfZero, Range.Low, Dozen.Second, Column.Third),
                new Number(29, Color.Black, Parity.Odd, Sector.NeighborsOfZero, Range.High, Dozen.Third, Column.Second),
                new Number(7, Color.Red, Parity.Odd, Sector.NeighborsOfZero, Range.Low, Dozen.First, Column.First),
                new Number(28, Color.Black, Parity.Even, Sector.NeighborsOfZero, Range.High, Dozen.Third, Column.First),
                new Number(12, Color.Red, Parity.Even, Sector.ZeroGame, Range.Low, Dozen.First, Column.Third),
                new Number(35, Color.Black, Parity.Odd, Sector.ZeroGame, Range.High, Dozen.Third, Column.Second),
                new Number(3, Color.Red, Parity.Odd, Sector.ZeroGame, Range.Low, Dozen.First, Column.Third),
                new Number(26, Color.Black, Parity.Odd, Sector.ZeroGame, Range.High, Dozen.Third, Column.Second)
            };

            return numbers;
        }
    }
}
